import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})

export class HttpClientService {

    private wsUrl:string="https://jsonplaceholder.typicode.com/users";

    constructor(private http:HttpClient) {}
    listUsers = [];

    executeHttpClient() {
        this.http.get(this.wsUrl).subscribe(
            
            never => { for (let key in never) { this.listUsers.push(never[key])}; },
            err => console.log('KO', err)
        ); 
    }

}