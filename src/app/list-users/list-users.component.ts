import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClientService } from '../HttpClient.service';
import { User } from 'src/model/user';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  allUsers = [];
  show = false;
  selectedUser: User;

  constructor(private httpClientService:HttpClientService) { }


  ngOnInit(): void {
  }

    generateListUsers(event) {
      this.show = true;
      this.httpClientService.executeHttpClient();
      this.allUsers = this.httpClientService.listUsers;
  }

  showUser(user) {
    
    this.selectedUser = user;
  }

}

