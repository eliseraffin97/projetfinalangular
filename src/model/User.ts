export class User {
    name: string;
    email: string;
    address: {
        street: string, 
        suite: string,
        zipcode: string, 
        city: string
    };
    company: {
        name: string
    };
    phone: string;
    website: string;

    constructor(name: string, email: string, street: string, suite: string, zipcode: string, city: string, societe: string, phone: string, website: string) {
        this.name = name;
        this.email = email;
        this.address.city = city;
        this.address.street = street;
        this.address.suite = suite;
        this.address.zipcode = zipcode;
        this.company.name = societe;
        this.phone = phone;
        this.website = website;
    }


}